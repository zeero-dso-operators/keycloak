#!/usr/bin/python

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import json

from uuid import uuid4

import requests

from keycloak import KeycloakAdmin
from keycloak.exceptions import KeycloakGetError
from ansible.module_utils.basic import AnsibleModule


def run_module():
    module = AnsibleModule(
        argument_spec=dict(
            user=dict(type='str', required=True),
            group=dict(type='str', required=True),
            auth_keycloak_url=dict(type='str', required=True),
            auth_realm=dict(type='str', default="master"),
            auth_username=dict(type='str', required=True),
            auth_password=dict(type='str', required=True, no_log=True),
            verify=dict(type='bool', default=True)
        )
    )
    result = {}
    token, err = get_access_token(
        module.params['auth_keycloak_url'],
        module.params['auth_realm'],
        module.params['auth_username'],
        module.params['auth_password'],
    )
    if err is not None:
        result['failed'] = True
        result['msg'] = err
        module.exit_json(**result)  

    changed, err = add_user_to_group(
        module.params['auth_keycloak_url'],
        module.params['auth_realm'],
        module.params['user'],
        module.params['group'],
        token
    )
    if changed is True:
        result['changed'] = True

    if err is not None:
        result['failed'] = True
        result['msg'] = err
        module.exit_json(**result)  

    module.exit_json(**result)


def get_access_token(url, realm, username, password):
    url = '{0}/realms/{1}/protocol/openid-connect/token'.format(url, realm)
    data = {
        'grant_type': 'password',
        'client_id': 'admin-cli',
        'username': username,
        'password': password,
    }
    r = requests.post(url, data=data)
    if r.status_code != 200:
        return (None, 'failed to retrieve the api access token',)
    
    return (r.json()['access_token'], None,)


def get_user_id(url, realm, user, token):
    r = requests.get(
        '{0}/admin/realms/{1}/users'.format(url, realm),
        headers={'Authorization': 'Bearer {0}'.format(token)}
    )
    for u in r.json():
        if u['username'] == user:
            return u['id']


def get_group_id(url, realm, group, token):
    r = requests.get(
        '{0}/admin/realms/{1}/groups'.format(url, realm),
        headers={'Authorization': 'Bearer {0}'.format(token)}
    )
    for g in r.json():
        if g['name'] == group:
            return g['id']
    else:
        r = requests.post(
            '{0}/admin/realms/{1}/groups'.format(url, realm),
            data=json.dumps({'name': group}),
            headers={
                'Authorization': 'Bearer {0}'.format(token),
                'Content-Type': 'application/json'
            }
        )
        return get_group_id(url, realm, group, token)


def add_user_to_group(url, realm, user, group, token):
    user_id = get_user_id(url, realm, user, token)
    group_id = get_group_id(url, realm, group, token)

    r = requests.get(
        '{0}/admin/realms/{1}/users/{2}/groups'.format(url, realm, user_id), 
        headers={'Authorization': 'Bearer {0}'.format(token)}
    )
    for g in r.json():
        if g['id'] == group_id:
            return (False, None,)

    r = requests.put(
        '{0}/admin/realms/{1}/users/{2}/groups/{3}'.format(url, realm, user_id, group_id),
        headers={'Authorization': 'Bearer {0}'.format(token)}
    )
    if r.status_code != 204:
        return (False, 'failed adding the group to the user',)

    return (True, None,)


def main():
    run_module()


if __name__ == '__main__':
    main()