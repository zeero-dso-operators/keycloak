#!/usr/bin/python

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from uuid import uuid4
from keycloak import KeycloakAdmin
from keycloak.exceptions import KeycloakGetError
from ansible.module_utils.basic import AnsibleModule


def run_module():
    module = AnsibleModule(
        argument_spec=dict(
            auth_keycloak_url=dict(type='str', required=True),
            auth_realm=dict(type='str', default="master"),
            auth_username=dict(type='str', required=True),
            auth_password=dict(type='str', required=True, no_log=True),
            client_id=dict(type='str', required=True),
            client_scopes=dict(type='list', default=["email", "profile", "roles", "web-origins"]),
            redirect_uris=dict(type='list', required=True),
            verify=dict(type='bool', default=True)
        )
    )
    admin = KeycloakAdmin(
        server_url='{url}/'.format(url=module.params['auth_keycloak_url']),
        username=module.params['auth_username'],
        password=module.params['auth_password'],
        realm_name=module.params['auth_realm'],
        verify=module.params['verify']
    )
    result = {}
    secret = str(uuid4())
    client = {
        'clientId': module.params['client_id'],
        'secret': secret,
        'surrogateAuthRequired':False,
        'enabled':True,
        'alwaysDisplayInConsole':False,
        'clientAuthenticatorType':'client-secret',
        'redirectUris': module.params['redirect_uris'],
        'notBefore':0,
        'bearerOnly':False,
        'consentRequired':False,
        'standardFlowEnabled':True,
        'implicitFlowEnabled':False,
        'directAccessGrantsEnabled':True,
        'serviceAccountsEnabled':False,
        'publicClient':False,
        'frontchannelLogout':False,
        'protocol':'openid-connect',
        'attributes':{},
        'authenticationFlowBindingOverrides':{},
        'fullScopeAllowed':True,
        'nodeReRegistrationTimeout':-1,
        'defaultClientScopes': module.params['client_scopes'],
        'optionalClientScopes':[
            'address',
            'phone',
            'offline_access',
            'microprofile-jwt'
        ],
        'access':{
            'view':True,
            'configure':True,
            'manage':True
        }
        
    }
    try:
        for c in admin.get_clients():
            if c['clientId'] == module.params['client_id']:
                break
        admin.create_client(client)
        result['client_secret'] = secret
        result['changed'] = True
    except KeycloakGetError as err:
        if err.response_code != 409:
            result['failed'] = True
            result['message'] = err.error_message

    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()