#!/usr/bin/python

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import json

from hashlib import md5
from uuid import uuid4

import requests

from keycloak import KeycloakAdmin
from keycloak.exceptions import KeycloakGetError
from ansible.module_utils.basic import AnsibleModule


REQUIRED_ACTIONS = [
    'Configure OTP',
    'Update Password'
]


def run_module():
    module = AnsibleModule(
        argument_spec=dict(
            username=dict(type='str', required=True),
            password=dict(type='str', required=True, no_log=True),
            auth_keycloak_url=dict(type='str', required=True),
            auth_realm=dict(type='str', default="master"),
            auth_username=dict(type='str', required=True),
            auth_password=dict(type='str', required=True, no_log=True),
            verify=dict(type='bool', default=True)
        )
    )
    admin = KeycloakAdmin(
        server_url='{url}/'.format(url=module.params['auth_keycloak_url']),
        username=module.params['auth_username'],
        password=module.params['auth_password'],
        realm_name=module.params['auth_realm'],
        verify=module.params['verify']
    )
    result = {}
    token, err = get_access_token(
        module.params['auth_keycloak_url'],
        module.params['auth_realm'],
        module.params['auth_username'],
        module.params['auth_password'],
    )
    if err is not None:
        result['failed'] = True
        result['msg'] = err
        module.exit_json(**result)  

    changed, err = add_required_user_actions(
        module.params['auth_keycloak_url'],
        module.params['auth_realm'],
        token
    )
    if err is not None:
        result['failed'] = True
        result['msg'] = err
        module.exit_json(**result)  
    elif changed: 
        result['changed'] = True

    changed, err = create_default_user(admin, module.params['username'], module.params['password'])
    if err is not None:
        result['failed'] = True
        result['msg'] = err
        module.exit_json(**result)
    elif changed: 
        result['changed'] = True

    changed, err = create_admin_role_mapping(
        module.params['username'], 
        module.params['auth_keycloak_url'],
        module.params['auth_realm'],
        token
    )
    if err is not None:
        result['failed'] = True
        result['msg'] = err
        module.exit_json(**result)
    elif changed:
        result['changed'] = True

    module.exit_json(**result)


def create_default_user(admin, username, password):
    try:
        for u in admin.get_users():
            if u['username'] == username:
                return (False, None,)
        else:
            user = {"username": username, "enabled": True}
            admin.create_user(user)
            for u in admin.get_users():
                if u['username'] == username:
                    password = md5(password.encode()).hexdigest()[:10]
                    admin.set_user_password(u['id'], password, temporary=True)
            return (True, None,)
    except KeycloakGetError as err:
        if err.response_code != 409:
            return (False, err.error_message,)


def create_admin_role_mapping(username, url, realm, token):
    r = requests.get(
        '{0}/admin/realms/{1}/users'.format(url, realm),
        headers={'Authorization': 'Bearer {0}'.format(token)}
    )
    user_id = None
    for u in r.json():
        if u['username'] == username:
            user_id = u['id']

    r = requests.get(
        '{0}/admin/realms/{1}/users/{2}/role-mappings/realm/available'.format(url, realm, user_id),
        headers={'Authorization': 'Bearer {0}'.format(token)}
    )
    role = None
    for _r in r.json():
        if _r['name'] == 'admin':
            role = _r
            break
    else:
        return (False, None,)

    r = requests.post(
        '{0}/admin/realms/{1}/users/{2}/role-mappings/realm'.format(url, realm, user_id),
        data=json.dumps([role]),
        headers={
            'Authorization': 'Bearer {0}'.format(token),
            'Content-Type': 'application/json'
        },
    )
    if r.status_code != 204:
        return (False, 'failed create the admin user mapping',)
    else:
        return (True, None,)


def add_required_user_actions(url, realm, token):
    changed = False
    r = requests.get(
        '{0}/admin/realms/{1}/authentication/required-actions'.format(url, realm),
        headers={'Authorization': 'Bearer {0}'.format(token)}
    )
    for action in r.json():
        if action['name'] in REQUIRED_ACTIONS and not action['defaultAction']:
            action['defaultAction'] = True
            action_url = '{0}/admin/realms/{1}/authentication/required-actions/{2}'.format(url, realm, action['alias'])
            r = requests.put(
                action_url,
                data=json.dumps(action),
                headers={
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer {0}'.format(token)
                }
            )
            if r.status_code != 204:
                return (False, 'failed to add the required action: %s' % action['alias'],)
            else:
                changed = True

    return (changed, None,)


def get_access_token(url, realm, username, password):
    url = '{0}/realms/{1}/protocol/openid-connect/token'.format(url, realm)
    data = {
        'grant_type': 'password',
        'client_id': 'admin-cli',
        'username': username,
        'password': password,
    }
    r = requests.post(url, data=data)
    if r.status_code != 200:
        return (None, 'failed to retrieve the api access token',)
    
    return (r.json()['access_token'], None,)


def main():
    run_module()


if __name__ == '__main__':
    main()