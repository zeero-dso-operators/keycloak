#!/usr/bin/python

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from uuid import uuid4
from keycloak import KeycloakAdmin
from keycloak.exceptions import KeycloakGetError
from ansible.module_utils.basic import AnsibleModule


def run_module():
    module = AnsibleModule(
        argument_spec=dict(
            auth_keycloak_url=dict(type='str', required=True),
            auth_realm=dict(type='str', default="master"),
            auth_username=dict(type='str', required=True),
            auth_password=dict(type='str', required=True, no_log=True),
            verify=dict(type='bool', default=True)
        )
    )
    admin = KeycloakAdmin(
        server_url='{url}/'.format(url=module.params['auth_keycloak_url']),
        username=module.params['auth_username'],
        password=module.params['auth_password'],
        realm_name=module.params['auth_realm'],
        verify=module.params['verify']
    )
    result = {}
    client_scope_id = str(uuid4())
    client_scope = {
        'id': client_scope_id,
        'name': 'groups',
        'protocol': 'openid-connect',
        'attributes': {
            'include.in.token.scope': 'true',
            'display.on.consent.screen': 'true'
        },
    }
    try:
        admin.create_client_scope(client_scope)
    except KeycloakGetError as err:
        if err.response_code != 409:
            result['failed'] = True
            result['message'] = err.error_message
            module.exit_json(**result)
    else:
        protocol_mapper_id = str(uuid4())
        protocol_mapper ={
            'id': protocol_mapper_id,
            'name': 'groups',
            'protocol': 'openid-connect',
            'protocolMapper': 'oidc-group-membership-mapper',
            'consentRequired': False,
            'config': {
                'full.path': 'false',
                'id.token.claim': 'true',
                'access.token.claim': 'true',
                'claim.name': 'groups',
                'userinfo.token.claim': 'true'
            }
        }

        try:
            admin.add_mapper_to_client_scope(client_scope_id, protocol_mapper)
        except KeycloakGetError as err:
            result['failed'] = True
            result['message'] = err.error_message
            module.exit_json(**result)
        else:
            result['changed'] = True

    module.exit_json(**result)

def main():
    run_module()


if __name__ == '__main__':
    main()