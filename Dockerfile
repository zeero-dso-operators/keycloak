FROM quay.io/operator-framework/ansible-operator:v1.0.0

USER root

RUN curl -LO https://get.helm.sh/helm-v3.4.1-linux-amd64.tar.gz && \
    tar zxf helm-v3.4.1-linux-amd64.tar.gz && \
    mv linux-amd64/helm /usr/local/bin/helm

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

USER ansible

COPY requirements.yml ${HOME}/requirements.yml
RUN ansible-galaxy collection install -r ${HOME}/requirements.yml \
 && chmod -R ug+rwx ${HOME}/.ansible

COPY lib ${HOME}/lib
COPY watches.yaml ${HOME}/watches.yaml
COPY roles/ ${HOME}/roles/
COPY playbooks/ ${HOME}/playbooks/

ENV ANSIBLE_LIBRARY=${HOME}/lib
